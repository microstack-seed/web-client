/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import LoadingIndicator from 'components/LoadingIndicator';
import H2 from 'components/H2';

// Imported to make graph calls
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { FormattedMessage } from 'react-intl';
import Section from './Section';
import messages from './messages';

// Example Query for apollo
const GET_USERS_QUERY = gql`
  query getUsers {
    keycloak {
      users {
        id
        firstName
        fullName
        enabled
        emailVerified
        email
      }
    }
  }
`;

const HomePage = () => {
  // useQuery webhook for fetching data automatically
  const resp = useQuery(GET_USERS_QUERY);

  if (resp.loading) return <LoadingIndicator />;
  if (resp.error) return <p>Error :(</p>;

  return (
    <article>
      <Helmet>
        <title>Home Page</title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />
      </Helmet>
      <div>
        <Section>
          <H2>
            <FormattedMessage {...messages.apollo} />
          </H2>
          {resp.data.keycloak.users.map(({ id, fullName }) => (
            <div key={id}>
              <p>
                {id}: {fullName}
              </p>
            </div>
          ))}
        </Section>
      </div>
    </article>
  );
};

export default HomePage;
