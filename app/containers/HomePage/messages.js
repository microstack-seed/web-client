/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.containers.HomePage';

export default defineMessages({
  apollo: {
    id: `${scope}.apollo.message`,
    defaultMessage: '<No Language translation found>',
  },
});
