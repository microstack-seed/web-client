import React from 'react';
import LocaleToggle from 'containers/LocaleToggle';
import Wrapper from './Wrapper';

function Footer() {
  return (
    <Wrapper>
      <section>Section 1</section>
      <section>
        <LocaleToggle />
      </section>
      <section>Section 2</section>
    </Wrapper>
  );
}

export default Footer;
