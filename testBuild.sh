#!/bin/sh

REACT_APP_HASURA_ENDPOINT="https://api.demo.udig.com/v1/graphql"
REACT_APP_KEYCLOAK_AUTH_URL="https://auth.demo.udig.com/auth"
REACT_APP_KEYCLOAK_REALM="microstack_seed"
REACT_APP_KEYCLOAK_CLIENT_ID="web-client"

docker build -t cms-web:dev .